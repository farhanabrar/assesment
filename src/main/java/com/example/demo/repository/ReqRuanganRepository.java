package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.ReqRuangan;

public interface ReqRuanganRepository extends JpaRepository<ReqRuangan, Integer>{
	
	@Query(value="SELECT * FROM request_ruangan WHERE ruangan_id = ?1", nativeQuery = true)
	Optional<ReqRuangan> findRuanganId(int id);
	
	@Query(value="SELECT * FROM request_ruangan WHERE id = ?1", nativeQuery = true)
	Optional<ReqRuangan> findById(int id);
	
	@Query(value="SELECT * FROM request_ruangan WHERE status_id = ?1", nativeQuery = true)
	List<ReqRuangan> findByStatus(int status);
}
