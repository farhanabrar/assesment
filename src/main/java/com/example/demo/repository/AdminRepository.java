package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, Integer>{
	
	@Query(value="SELECT * FROM admin WHERE id = ?1 AND secret_code = ?2", nativeQuery = true)
	Optional<Admin> findAdminByID(int id, String code);
	
	@Query(value="SELECT * FROM admin WHERE id = ?1", nativeQuery = true)
	Optional<Admin> findById(int id);
}
