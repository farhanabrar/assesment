package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Ruangan;

public interface RuanganRepository extends JpaRepository<Ruangan, Integer> {
	
	Optional<Ruangan> findRuanganByid(int id);
	
	Page<Ruangan> findAll(Pageable page);
}
