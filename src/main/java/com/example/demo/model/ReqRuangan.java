package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "request_ruangan")
public class ReqRuangan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "ruangan_id")
	private int ruanganId;
	
	@Column(name = "tgl_mulai")
	private Date tglMulai;
	
	@Column(name="tgl_selesai")
	private Date tglSelesai;
	

	@Column(name = "mahasiswa_id")
	private int mahasiswaId;
	
	@Column(name = "admin_id")
	private int adminId;

	@Column(name = "status_id")
	private int statusId;
	
	@ManyToOne
	@JoinColumn(name="mahasiswa_id", insertable = false, updatable = false)
	public Mahasiswa mahasiswa;
	
	
	@ManyToOne
	@JoinColumn(name="status_id", insertable = false, updatable = false)
	public Status status;
	
	@ManyToOne
	@JoinColumn(name="ruangan_id", insertable = false, updatable = false)
	public Ruangan ruangan ;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public int getRuanganId() {
		return ruanganId;
	}

	public void setRuanganId(int ruanganId) {
		this.ruanganId = ruanganId;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	public int getMahasiswaId() {
		return mahasiswaId;
	}

	public void setMahasiswaId(int mahasiswaId) {
		this.mahasiswaId = mahasiswaId;
	}

	public Mahasiswa getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(Mahasiswa mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public Date getTglMulai() {
		return tglMulai;
	}

	public void setTglMulai(Date tglMulai) {
		this.tglMulai = tglMulai;
	}

	public Date getTglSelesai() {
		return tglSelesai;
	}

	public void setTglSelesai(Date tglSelesai) {
		this.tglSelesai = tglSelesai;
	}
	
	

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Ruangan getRuangan() {
		return ruangan;
	}

	public void setRuangan(Ruangan ruangan) {
		this.ruangan = ruangan;
	}
	
	
	
}
