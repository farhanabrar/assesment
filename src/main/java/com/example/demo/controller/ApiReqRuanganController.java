package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Admin;
import com.example.demo.model.ReqRuangan;
import com.example.demo.repository.AdminRepository;
import com.example.demo.repository.ReqRuanganRepository;


@RestController
@CrossOrigin("*")
@RequestMapping("/transaction/")
public class ApiReqRuanganController {

	@Autowired
	private ReqRuanganRepository reqRuanganRepository;
	
	@Autowired
	private AdminRepository adminRepository;

	@PostMapping("/req-ruangan")
	public ResponseEntity<Object> requestreqRuangan(@RequestBody ReqRuangan reqRuangan) {
		int idRuangan = reqRuangan.getRuanganId();
		Optional<ReqRuangan> cekRuangan = this.reqRuanganRepository.findRuanganId(idRuangan);
		if (cekRuangan.isPresent() && cekRuangan.get().getStatusId() !=3) {
			return new ResponseEntity<>("Ruangan sudah di Book", HttpStatus.CONFLICT);
		} else {
			reqRuangan.setStatusId(1);
			this.reqRuanganRepository.save(reqRuangan);
			return new ResponseEntity<>("booking succes ", HttpStatus.OK);
		}

	}

	@GetMapping("/all-reqRuangan")
	public ResponseEntity<List<Map<String, Object>>> getAllreqRuangan(@RequestParam(defaultValue = "0") int status) {
		try {
			List<Map<String, Object>> response = new ArrayList<>();
			if(status == 0) {
				List<ReqRuangan> listreqRuangan = this.reqRuanganRepository.findAll();
				response = listreqRuangan.stream().map(x -> mapping(x)).collect(Collectors.toList());
			}else {
				List<ReqRuangan> listreqRuangan = this.reqRuanganRepository.findByStatus(status);
				response = listreqRuangan.stream().map(x -> mapping(x)).collect(Collectors.toList());
			}
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}

	}

	public Map<String, Object> mapping(ReqRuangan ruang) {
		Map<String, Object> response = new HashMap<>();
		response.put("id", ruang.getId());
		response.put("nama Ruangan", ruang.getRuangan().getNamaRuangan());
		response.put("mahasiswa", ruang.getMahasiswa().getNamaMahasiswa());
		if(ruang.getAdminId() != 0 ) { 
			Optional<Admin> adminData = adminRepository.findById(ruang.getAdminId());
			response.put("approved by", adminData.get().getNama());
		}		
		response.put("status", ruang.getStatus().getStatusName());
		return response;
	}

	@PutMapping("/update-status")
	public ResponseEntity<Object> updateStatus(@RequestParam("secret_code") String secretCode,
			@RequestParam("admin_id") int adminId,  @RequestParam("reqRuangan_id") int id, @RequestParam("status_id") int statusId) {
		Optional<Admin> adminData = this.adminRepository.findAdminByID(adminId, secretCode);
		if(adminData.isPresent()) {
			Optional<ReqRuangan> reqRuanganData = this.reqRuanganRepository.findById(id);
			if(reqRuanganData.isPresent()) {
				ReqRuangan reqRuangan = new ReqRuangan();
				if(statusId == 3 || statusId == 1) {
					reqRuangan.setAdminId(0);
				}else {
					reqRuangan.setAdminId(adminId);
				}
				reqRuangan.setId(id);
				reqRuangan.setStatusId(statusId);
				reqRuangan.setRuanganId(reqRuanganData.get().getRuanganId());
				reqRuangan.setMahasiswaId(reqRuanganData.get().getMahasiswaId());
				this.reqRuanganRepository.save(reqRuangan);
				return new ResponseEntity<>("update success", HttpStatus.OK);
			}else {
				return new ResponseEntity<>("request ruangan not found", HttpStatus.NOT_FOUND);
			}
		}else {
			return new ResponseEntity<>("id dan/atau secret code salah", HttpStatus.FORBIDDEN);
		}
	}

}
