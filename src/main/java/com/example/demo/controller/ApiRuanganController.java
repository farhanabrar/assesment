package com.example.demo.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Ruangan;
import com.example.demo.repository.RuanganRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/master/ruangan")
public class ApiRuanganController {
	
	@Autowired
	private RuanganRepository ruanganRepository;
	
	@GetMapping("/all-ruangan")
	public ResponseEntity<Map<String, Object>> getAllRuangan(@RequestParam(defaultValue = "1") int currentPage,
			@RequestParam(defaultValue = "5") int limit) {
		
		try {
			List<Ruangan> ruangan = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(currentPage-1, limit);
			
			Page<Ruangan> pages = this.ruanganRepository.findAll(pagingSort);
			
			ruangan = pages.getContent();
			
			Map<String, Object> response = new HashMap<>();
			
			response.put("page", pages.getNumber()+1);
			response.put("total", pages.getTotalElements());
			response.put("per_page", limit);
			response.put("total_pages", pages.getTotalPages());
			response.put("data", ruangan);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
